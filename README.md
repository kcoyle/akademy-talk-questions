# Akademy Talk Questions

This repository contains a pragmatic solution for real-time q&a during remote conferences.

# Tech Stack

This app has been written using [meteor.js](https://www.meteor.com/)

## Getting Started

```
# Install meteor
$ npm install -g meteor

# Install dependencies
$ meteor npm install

# Run meteor
$ meteor npm install
```