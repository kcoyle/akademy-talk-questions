const fetch = require('node-fetch');
const slugify = require('slugify')

const eventDataUrl = 'https://conf.kde.org/export/event/1.json?detail=contributions';

const getTalks = async () => {
  const request = await fetch(eventDataUrl);

  const data = await request.json();

//   console.log(data.results[0].contributions[0]); process.exit();

//   return data.results[0].contributions.map(talk => ({
//       id: talk.url.replace('https://conf.kde.org/event/1/contributions/', '').replace('/', ''),
//       startDate: talk.startDate,
//       endDate: talk.endDate,
//       title: talk.title,
//       track: talk.track,
//       description: talk.description,
//       speakers: talk.speakers.map(speaker => `${speaker.first_name} ${speaker.last_name}`).join(', ')
//   })).sort((a, b) => {
//       if(!a.startDate) {
//         return -1;
//       }
//       if(!b.startDate) {
//         return 1;
//       }
//       const dateA = new Date(`${a.startDate.date} ${a.startDate.time}`);
//       const dateB = new Date(`${b.startDate.date} ${b.startDate.time}`);

//       return dateA - dateB
//   });

return data.results[0].contributions.map(talk => ({
    // id: talk.url.replace('https://conf.kde.org/event/1/contributions/', '').replace('/', ''),
    // startDate: talk.startDate,
    // endDate: talk.endDate,
    type: 'Video',
    name: talk.title,
    licence: 1,
    category: 15,
    // track: talk.track,
    description: talk.description,
    speakers: talk.speakers.map(speaker => `${speaker.first_name} ${speaker.last_name}`).join(', ')
})).sort((a, b) => {
    if(!a.startDate) {
      return -1;
    }
    if(!b.startDate) {
      return 1;
    }
    const dateA = new Date(`${a.startDate.date} ${a.startDate.time}`);
    const dateB = new Date(`${b.startDate.date} ${b.startDate.time}`);

    return dateA - dateB
});
}

getTalks().then(talks => {
    talks
        .filter(talk => !!talk.track)
        .filter(talk => talk.id !== "51")
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-18')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-21')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-22')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-23')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-24')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-25')
        .forEach(talk => {
            // console.log([
            //     talk.id,
            //     talk.startDate ? talk.startDate.date : '',
            //     talk.track,
            //     talk.title,
            //     talk.speakers,
            //     // talk.startDate.date,
            //     // talk.startDate.time
            // ].join(', '));

            const command = `mv "${talk.id}.mp4" "akademy2021-${talk.id}-${slugify(talk.title)}.mp4"`
            console.log(command);
    });
});