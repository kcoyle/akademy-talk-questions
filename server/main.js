import { Meteor } from 'meteor/meteor';

import jwt from 'jsonwebtoken';

import { RoomsCollection } from '/imports/api/rooms';
import { QuestionsCollection } from '/imports/api/questions';

import fetch from 'node-fetch';

const eventDataUrl = 'https://conf.kde.org/export/event/1.json?detail=contributions';

const {
  AUTH_SECRET = 'changeme',
  AUTH_PASSWORD = 'P@ssw0rd'
} = process.env;

const talks = [];

const getTalks = async () => {
  const request = await fetch(eventDataUrl);

  const data = await request.json();

  data.results[0].contributions.map(talk => ({
      startDate: talk.startDate,
      endDate: talk.endDate,
      title: talk.title,
      track: talk.track,
  })).sort((a, b) => {
      if(!a.startDate) {
        return -1;
      }
      if(!b.startDate) {
        return 1;
      }
      const dateA = new Date(`${a.startDate.date} ${a.startDate.time}`);
      const dateB = new Date(`${b.startDate.date} ${b.startDate.time}`);

      return dateA - dateB
  })
  .forEach(talk => {
    talks.push(talk);
  });
}

const createRooms = async () => {
  talks.forEach(talk => {
    RoomsCollection.insert({
      name: talk.title,
    });
  })
}

const rooms = RoomsCollection.find({}).fetch();

getTalks()
  .then(() => {
    if(rooms.length === 0) {
      createRooms();
    }
  })

Meteor.startup(() => {});

Meteor.methods({
  'auth.login'({ password }) {
    if (password === AUTH_PASSWORD) {
      const token = jwt.sign({ success: true }, AUTH_SECRET);
      return token;
    }
  },
  'questions.deleteQuestion'({ questionId, authtoken }) {
    jwt.verify(authtoken, AUTH_SECRET, (err, decoded) => {
      if (decoded) {
        QuestionsCollection.remove({
          _id: questionId,
        })
      }
    });
  },
  'talks.list'() {
    return talks;
  },
});
