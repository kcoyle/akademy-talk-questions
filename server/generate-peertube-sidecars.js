const fs = require('fs');
const fetch = require('node-fetch');
const slugify = require('slugify')

const eventDataUrl = 'https://conf.kde.org/export/event/1.json?detail=contributions';
const s3RootUrl = 'https://akademy2021.s3.eu-west-1.amazonaws.com/videos_complete';

const getTalks = async () => {
  const request = await fetch(eventDataUrl);

  const data = await request.json();

  return data.results[0].contributions.map(talk => ({
    id: talk.url.replace('https://conf.kde.org/event/1/contributions/', '').replace('/', ''),
    startDate: talk.startDate,
    name: talk.title,
    track: talk.track,
    description: talk.description,
})).sort((a, b) => {
    if(!a.startDate) {
      return -1;
    }
    if(!b.startDate) {
      return 1;
    }
    const dateA = new Date(`${a.startDate.date} ${a.startDate.time}`);
    const dateB = new Date(`${b.startDate.date} ${b.startDate.time}`);

    return dateA - dateB
});
}

getTalks().then(talks => {
    talks
        .filter(talk => !!talk.track)
        .filter(talk => talk.id !== "51")
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-18')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-21')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-22')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-23')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-24')
        .filter(talk => talk.startDate && talk.startDate.date !== '2021-06-25')
        .forEach(talk => {
            const filename = `${s3RootUrl}/akademy2021-${talk.id}-${slugify(talk.name).replace(':', '')}.mp4`
            const jsonFileName = `${talk.id}-${slugify(talk.name).replace(':', '')}.json`;

            fs.writeFileSync(jsonFileName, JSON.stringify({
                type: 'Video',
                name: talk.name,
                description: talk.description,
            }));

            const command = `import -c akademy -j ${jsonFileName} ${filename}`;
            console.log(command);
    });
});