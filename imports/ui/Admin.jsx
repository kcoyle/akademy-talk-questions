import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';

import { useTracker } from 'meteor/react-meteor-data';
import { UsersCollection } from '../api/users';

export const Admin = ({setLoggedIn}) => {
    const [username, setUsername] = useState();
    const [adminPassword, setadminPassword] = useState();

    const localStorage = window.localStorage;

    const additionalPath = window.location.pathname.indexOf('akademy-talk-questions')
    ? '/akademy-talk-questions'
    : '';
  
    const login = (e) => {
      e.preventDefault();

      Meteor.call('auth.login', {
        password: adminPassword,
      }, (_, result) => {
        if(result) {
          localStorage.sessionToken = result;
          
          const userId = UsersCollection.insert({
            username,
          });
      
          storeName({userId, username});

          window.location = `${additionalPath}/`;
        } else {
          alert("Login Failed")
        }
      });
    }
  
    const storeName = ({userId, username}) => {
      localStorage.userId = userId;
      localStorage.username = username;
      localStorage.admin = true;
    }
  
    return (
      <div>
        <form action="#" onSubmit={login}>
          <div className="form-group">
              <label>Please tell us your name</label>
              <input className="form-control" value={username} onChange={e => setUsername(e.target.value)} />
              <label>Admin Password</label>
              <FormControl className="form-control" type="password" value={adminPassword} onChange={e => setadminPassword(e.target.value)} />
              <Button type="submit" className="btn btn-primary" >Login</Button>
          </div>
        </form>
      </div>
    );
};
