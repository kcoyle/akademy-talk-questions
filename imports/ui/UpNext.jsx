import React, { useState, useEffect } from 'react';

import { Container, Row, Col, Button, Card } from 'react-bootstrap';

export const UpNext = ({ hideable, hidden, heading }) => {
  const [isHidden, setIsHidden] = useState(hidden);
  const [talks, setTalks] = useState([]);
  const [lastTalkRoom1, setLastTalkRoom1] = useState(false);
  const [lastTalkRoom2, setLastTalkRoom2] = useState(false);
  const [nextTalkRoom1, setNextTalkRoom1] = useState(false);
  const [nextTalkRoom2, setNextTalkRoom2] = useState(false);
  const [timer, setTimer] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
       setTimer(new Date());
    }, 10000);
 
    return () => clearInterval(interval);
 
  }, []);

  const talkList = [];
  if (!talks.length) {
    Meteor.call('talks.list', {}, (_, talks) => {
        setTalks(talks);
    
        talks.forEach(talk => {
            if(talk.startDate === null) {
                return;
            }
      
            talkList.push({
                scheduledAt: new Date(Date.UTC(
                  talk.startDate.date.substring(0, 4),
                  talk.startDate.date.substring(5, 7) - 1,
                  talk.startDate.date.substring(8, 10),
                  talk.startDate.time.substring(0, 2),
                  talk.startDate.time.substring(3, 5),
                  talk.startDate.time.substring(6, 8),
                  0)),
                finishedAt: new Date(Date.UTC(
                  talk.endDate.date.substring(0, 4),
                  talk.endDate.date.substring(5, 7) - 1,
                  talk.endDate.date.substring(8, 10),
                  talk.endDate.time.substring(0, 2),
                  talk.endDate.time.substring(3, 5),
                  talk.endDate.time.substring(6, 8),
                  0)),
                ...talk,
            });
        });

        // console.log(talkList);
      
        const currentTime = new Date(new Date());

        // const today = new Date()
        // const tomorrow = new Date(today)
        // tomorrow.setDate(tomorrow.getDate() + 1)

        // const currentTime = tomorrow;

        const measuredTalkList = talkList.map(talk => {
            // console.log(talk, talk.scheduledAt, currentTime);
            if (talk.scheduledAt < currentTime) {
                talk.status = 'past';
            }
            if (talk.scheduledAt > currentTime) {
              talk.status = 'future';
            }
            if (talk.scheduledAt <= currentTime && talk.finishedAt > currentTime) {
              talk.status = 'current';
          }
      
          return talk;
        });
      
        const calculatedLastTalkRoom1 = measuredTalkList
          .filter(talk => talk.status === 'current')
          .filter(talk => talk.track === 'Track 1')
          .sort((a, b) => b.scheduledAt - a.scheduledAt)[0];
      
        setLastTalkRoom1(calculatedLastTalkRoom1);

        const calculatedLastTalkRoom2 = measuredTalkList
          .filter(talk => talk.status === 'current')
          .filter(talk => talk.track === 'Track 2')
          .sort((a, b) => b.scheduledAt - a.scheduledAt)[0];
      
        setLastTalkRoom2(calculatedLastTalkRoom2);
      
        const calculatedNextTalkRoom1 = measuredTalkList
          .filter(talk => talk.status === 'future')
          .filter(talk => talk.track === 'Track 1')
          .sort((a, b) => a.scheduledAt - b.scheduledAt)[0];
      
        setNextTalkRoom1(calculatedNextTalkRoom1);

        const calculatedNextTalkRoom2 = measuredTalkList
          .filter(talk => talk.status === 'future')
          .filter(talk => talk.track === 'Track 2')
          .sort((a, b) => a.scheduledAt - b.scheduledAt)[0];
      
        setNextTalkRoom2(calculatedNextTalkRoom2);
      });
  }

  return (
    <Container refreshState={timer}>
      {/* <div>{String(timer)}</div> */}
      {heading && <Row>
          <Col>
            <h4>Now / Next</h4>
          </Col>
        </Row>}
      { hideable && isHidden && <>
          <Row>
            <Col>
              <Button size="sm" style={{
                float: 'right',
              }} onClick={() => { setIsHidden(false) }}>Show Schedule Information</Button>
            </Col>
          </Row>
        </>}
        { hideable && !isHidden && <>
          <Row>
            <Col>
              <Button size="sm" style={{
                float: 'right',
              }} onClick={() => { setIsHidden(true) }}>Hide Schedule Information</Button>
            </Col>
          </Row>
          <Row>
            <Col>
              &nbsp;
            </Col>
          </Row>
        </>}
      { !isHidden && <>
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title style={{ fontSize: 12, }}>Room 1: Current Talk</Card.Title>
                <Card.Text>
                  <strong>{!!lastTalkRoom1 ? `${lastTalkRoom1.startDate.time.substring(0,5)}` + ' ' : ''}</strong>
                  {!!lastTalkRoom1 ? `${lastTalkRoom1.title}` : 'Stay Tuned.'}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title style={{ fontSize: 12, }}>Room 2: Current Talk</Card.Title>
                <Card.Text>
                  <strong>{!!lastTalkRoom2 ? `${lastTalkRoom2.startDate.time.substring(0,5)}` + ' ' : ''}</strong>
                  {!!lastTalkRoom2 ? `${lastTalkRoom2.title}` : 'Stay Tuned.'}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            &nbsp;
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title style={{ fontSize: 12, }}>Room 1: Next Talk</Card.Title>
                <Card.Text>
                  <strong>{!!nextTalkRoom1 ? `${nextTalkRoom1.startDate.time.substring(0,5)}` + ' ' : ''}</strong>
                  {!!nextTalkRoom1 ? `${nextTalkRoom1.title}` : 'Stay Tuned.'}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title style={{ fontSize: 12, }}>Room 2: Next Talk</Card.Title>
                <Card.Text>
                  <strong>{!!nextTalkRoom2 ? `${nextTalkRoom2.startDate.time.substring(0,5)}` + ' ' : ''}</strong>
                  {!!nextTalkRoom2 ? `${nextTalkRoom2.title}` : 'Stay Tuned.'}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </>}
    </Container>
  );
};
