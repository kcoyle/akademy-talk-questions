import React, { useState } from 'react';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { useTracker } from 'meteor/react-meteor-data';
import { UsersCollection } from '../api/users';

export const Login = ({setloggedIn}) => {
  const [username, setUsername] = useState('');

  const localStorage = window.localStorage;

  const login = (e) => {
    e.preventDefault();

    if (username.length <= 3) {
      return;
    }

    const userId = UsersCollection.insert({
        username,
    });

    storeName({userId, username});
    setloggedIn(true);
  }

  const storeName = ({userId, username}) => {
    localStorage.userId = userId
    localStorage.username = username
  }

  return (
    <div>
      <h1>Akademy 2021 Q&A</h1>
      <Form action="#" onSubmit={login}>
        <Form.Group className="mb-3">
          <Form.Label>Please tell us your name.</Form.Label>
          <div className="row">
            <div className="col-md-3">
              <Form.Control value={username} onChange={e => setUsername(e.target.value)} />
            </div>
            <div className="col-md-3">
              <Button type="submit" variant="primary">Login</Button>
            </div>
            <em>You will not be given an opportunity to change this name during the event.</em>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
