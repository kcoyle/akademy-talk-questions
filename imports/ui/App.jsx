import React, { useState } from 'react';

import { UsersCollection } from '../api/users';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { Questions } from './Questions.jsx';
import { QuestionsAdmin } from './QuestionsAdmin.jsx';
import { Login } from './Login.jsx';
import { Welcome } from './Welcome.jsx';
import { UpNext } from './UpNext.jsx';
import { Admin } from './Admin.jsx';

export const App = () => {
  const pathname = window.location.pathname.replace('/akademy-talk-questions', '');

  const [loggedIn, setloggedIn] = useState(false);

  const localStorage = window.localStorage;

  if(window.location.href.indexOf('userid') > -1) {
    const username = window.location.href.split('userid=')[1].split('&')[0];

    const userId = UsersCollection.insert({
        username,
    });

    localStorage.userId = userId
    localStorage.username = username

    setLoggedIn = true;
  }

  if (pathname == '/comingup') {
    return (
      <>
        <UpNext hideable={false} heading={true} />
      </>
    );
  }
  
  if (pathname == '/admin') {
    return (
      <div>
        <h2>Admin Login</h2>
        <Admin setLoggedIn={setloggedIn}/>
      </div>
    );
  }

  if (pathname.indexOf('/admin') > -1) {
    return (
      <div>
        <QuestionsAdmin setLoggedIn={setloggedIn} />
      </div>
    );
  }

  return (
    <>
      <UpNext hideable={true} heading={false} hidden={true}/>
      <br />
      <Container>
        <Row md={12}>
          {loggedIn || localStorage.userId
            ? <div>
              {pathname.length > 3
                ? <>
                    <Questions/>
                  </>
                : <Welcome setloggedIn={setloggedIn} />
              }
            </div>
            : <div><Login setloggedIn={setloggedIn} /></div>}
        </Row>
      </Container>
    </>
  );
}
