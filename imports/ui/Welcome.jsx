import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';

import { useTracker } from 'meteor/react-meteor-data';
import { RoomsCollection } from '../api/rooms';

export const Welcome = ({ setloggedIn }) => {
  const [roomName, setRoomName] = useState('');

  const rooms = useTracker(() => {
    return RoomsCollection.find().fetch();
  });

  const addRoom = (e) => {
    e.preventDefault();

    if (roomName.length <= 3) {
      return;
    }

    RoomsCollection.insert({
      name: roomName,
    });

    setRoomName('');
  }

  const deleteRoom = (_id) => {
    RoomsCollection.remove({
      _id
    });
  }

  const logout = () => {
    delete localStorage.userId;
    delete localStorage.username;

    setloggedIn(false);
  }

  const additionalPath = window.location.pathname.indexOf('akademy-talk-questions')
    ? '/akademy-talk-questions'
    : '';

  return (
    <div>
        <h2>Akademy 2021 - Q&A - Rooms</h2>
        <Form action="#" onSubmit={addRoom}>
          <Form.Group className="mb-3">
            <Form.Label>Create a Room</Form.Label>
            <div className="row">
              <div className="col-md-3">
                <Form.Control value={roomName} onChange={e => setRoomName(e.target.value)} />
              </div>
              <div className="col-md-3">
                <Button type="submit" variant="primary" disabled={roomName.length <= 3}>Create Room</Button>
              </div>
            </div>
          </Form.Group>
        </Form>
        <h3>Available Rooms</h3>
        {rooms.length > 0
        ? <Table striped bordered hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Matrix Widget URL</th>
              {localStorage.admin === 'true' && <th>Admin URL</th>}
              {localStorage.admin === 'true' && <th>&nbsp;</th>}
            </tr>
          </thead>
          <tbody>
            {rooms.map(
              ({ _id, name }) => <tr key={_id}>
                  <td>{name}</td>
                  <td><a href={`${additionalPath}/${_id}`}>{window.location.protocol}//{window.location.host}{additionalPath}/{_id}</a></td>
                  {localStorage.admin === 'true' && <td><a href={`${additionalPath}/admin/${_id}`}>{window.location.protocol}//{window.location.host}{additionalPath}/admin/{_id}</a></td>}
                  {localStorage.admin === 'true' && <td>
                    <Button className="btn btn-primary" onClick={() => { deleteRoom(_id) }}>Delete</Button>
                    <span>&nbsp;</span>
                  </td>}
                </tr>
            )}
          </tbody>
        </Table>
        : <p>No rooms to show</p>}
      <Button variant="primary" href={`${additionalPath}/admin`}>Admin Login</Button>&nbsp;
      <Button variant="primary" href="#" onClick={logout}>Change Name</Button>
      <p>&nbsp;</p>
    </div>
  );
};
