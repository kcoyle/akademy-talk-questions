import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp, faThumbsDown, faTrash } from '@fortawesome/fontawesome-free-solid'

import { useTracker } from 'meteor/react-meteor-data';
import { QuestionsCollection } from '../api/questions';
import { RoomsCollection } from '../api/rooms';

export const Questions = () => {
  const pathname = window.location.pathname.replace('/akademy-talk-questions', '');

  const [questionText, setQuestionText] = useState('');
  const [votes, setVotes] = useState({});
  const [charactersRemaining, setCharactersRemaining] = useState(128);

  const localStorage = window.localStorage;

  const { userId, username } = localStorage;

  const roomId = pathname.replace('/', '').split('?')[0];

  console.log(roomId);

  const { name: roomName } = useTracker(() => {
    const rooms = RoomsCollection.find({
      _id: roomId,
    }).fetch();
    
    return rooms.length > 0
      ? rooms[0]
      : {};
  });
  
  const questions = useTracker(() => {
    return QuestionsCollection.find({
      room: roomId,
    }).fetch();
  });

  const addQuestion = (e) => {
    e.preventDefault();

    if (questionText.length <= 3) {
      return;
    }

    QuestionsCollection.insert({
      text: questionText,
      room: roomId,
      userId,
      username,
    });

    setQuestionText('');
  }

  const deleteQuestion = (_id) => {
    Meteor.call('questions.deleteQuestion', {
      questionId: _id,
      authtoken: localStorage.sessionToken,
    });
  }

  const canVote = ({ _id }) => {
    if (votes[_id] && votes[_id] > 3) {
      return false;
    }

    return true;
  }

  const vote = (_id, positive) => {
    const questions = QuestionsCollection.find({
      _id,
    }).fetch();

    if(questions.length) {
      const question = questions[0];

      if(!question.score) {
        question.score = 0;
      }

      positive
        ? question.score++
        : question.score--;

      QuestionsCollection.update(_id, question);

      if(!votes[_id]) {
        votes[_id] = 0;
      }
      votes[_id]++;
    }
  }

  return (
    <div>
      <h3 style={{
        marginTop: 15
      }}>{roomName}</h3>
      <Form action="#" onSubmit={addQuestion}>
        <Form.Group className="mb-3">
          <Form.Label>Ask a question</Form.Label>
          <div className="row">
            <div className="col-md-12">
              <Form.Control as="textarea" value={questionText} onChange={e => {
                if((192 - e.target.value.length) < 0) {
                  return;
                }

                setQuestionText(e.target.value);
                setCharactersRemaining(192 - e.target.value.length);
              }} />
              <p>Characters Remaining: {charactersRemaining}</p>
            </div>
            <div className="col-md-3" style={{ marginTop: -10 }}>
              <Button type="submit" variant="primary" disabled={questionText.length <= 3}>Add New Question</Button>
            </div>
          </div>
        </Form.Group>
      </Form>
      {questions.length > 0
        ? questions.map(({_id, text, username, score = 0}) => (<Card style={{
          marginBottom: 5
        }}>
          <Card.Body>
            <strong>{username}</strong> <em>Score: <strong>{score}</strong></em><br />
            {text}<br />
            <Button style={{ marginRight: 5 }} variant="primary" disabled={!canVote({ _id })} onClick={() => { vote(_id, true) }}><FontAwesomeIcon icon="thumbs-up" /></Button>
            <Button style={{ marginRight: 5 }} variant="primary" disabled={!canVote({ _id })} onClick={() => { vote(_id, false) }}><FontAwesomeIcon icon="thumbs-down" /></Button>
            {localStorage.admin === 'true'
              ? <Button variant="danger" onClick={() => { deleteQuestion(_id) }}><FontAwesomeIcon icon="trash" /></Button>
              : <span>&nbsp;</span>}
          </Card.Body>
        </Card>))
        : <p>No questions yet, anything you'd like to ask the speaker?</p>}
    </div>
  );
};