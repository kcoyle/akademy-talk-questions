import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp, faThumbsDown, faTrash } from '@fortawesome/fontawesome-free-solid'

import { useTracker } from 'meteor/react-meteor-data';
import { QuestionsCollection } from '../api/questions';
import { RoomsCollection } from '../api/rooms';

export const QuestionsAdmin = () => {
  const pathname = window.location.pathname.replace('/akademy-talk-questions', '');

  const [questionText, setQuestionText] = useState('');
  const [votes, setVotes] = useState({});

  const localStorage = window.localStorage;

  const { userId, username } = localStorage;

  const roomId = pathname.replace('/admin/', '').split('?')[0];

  const { name: roomName } = useTracker(() => {
    const rooms = RoomsCollection.find({
      _id: roomId,
    }).fetch();
    
    return rooms.length > 0
      ? rooms[0]
      : {};
  });
  
  const questions = useTracker(() => {
    return QuestionsCollection.find({
      room: roomId,
    }).fetch();
  });

  const addQuestion = (e) => {
    e.preventDefault();

    if (questionText.length <= 3) {
      return;
    }

    QuestionsCollection.insert({
      text: questionText,
      room: roomId,
      userId,
      username,
    });

    setQuestionText('');
  }

  const deleteQuestion = (_id) => {
    Meteor.call('questions.deleteQuestion', {
      questionId: _id,
      authtoken: localStorage.sessionToken,
    });
  }

  const canVote = ({ _id }) => {
    return true;
  }

  const vote = (_id, positive) => {
    const questions = QuestionsCollection.find({
      _id,
    }).fetch();

    if(questions.length) {
      const question = questions[0];

      if(!question.score) {
        question.score = 0;
      }

      positive
        ? question.score++
        : question.score--;

      QuestionsCollection.update(_id, question);

      if(!votes[_id]) {
        votes[_id] = 0;
      }
      votes[_id]++;
    }
  }

  if(localStorage.admin !== 'true') {
      return <p>Access Denied</p>
  }

  return (
    <div>
      <h3 style={{
        marginTop: 15
      }}>{roomName}</h3>
      <h5>Total Questions: {questions.length}</h5>
      <Form action="#" onSubmit={addQuestion}>
        <Form.Group className="mb-3">
          <Form.Label>Ask a question</Form.Label>
          <div className="row">
            <div className="col-md-3">
              <Form.Control type="textarea" value={questionText} onChange={e => setQuestionText(e.target.value)} />
            </div>
            <div className="col-md-3">
              <Button type="submit" variant="primary" disabled={questionText.length <= 3} onClick={addQuestion}>Add New Question</Button>
            </div>
          </div>
        </Form.Group>
      </Form>
      <br />
      <div class="row">
          <div className="col-md-6">
          <h3>Top Questions</h3>
            {questions.length > 0
                ? [ ...questions ].sort((a, b) => (b.score - a.score)).map(({_id, text, username, score = 0}) => (<Card style={{
                marginBottom: 5
                }}>
                <Card.Body>
                    <strong>{username}</strong><br />
                    {text}<br />
                    <strong>Score: {score}</strong>
                </Card.Body>
                </Card>))
                : <p>No questions yet, anything you'd like to ask the speaker?</p>}
            <br />
          </div>
          <div className="col-md-6">
          <h3>All Questions</h3>
            {questions.length > 0
                ? <Table striped bordered hover>
                <thead>
                    <tr>
                    {/* <th>_id</th>
                    <th>UserId</th> */}
                    <th style={{ minWidth: 200 }}>Username</th>
                    <th>text</th>
                    <th>score</th>
                    <th style={{ minWidth: 200 }}>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    {questions.map(
                    ({ _id, text, score = 0, username }) => <tr key={_id}>
                        <td>{username}</td>
                        <td>{text}</td>
                        <td>{score}</td>
                        <td>
                            <Button style={{ marginRight: 5 }} variant="primary" disabled={!canVote({ _id })} onClick={() => { vote(_id, true) }}><FontAwesomeIcon icon="thumbs-up" /></Button>
                            <Button style={{ marginRight: 5 }} variant="primary" disabled={!canVote({ _id })} onClick={() => { vote(_id, false) }}><FontAwesomeIcon icon="thumbs-down" /></Button>
                            {localStorage.admin === 'true'
                            ? <Button style={{ marginRight: 5 }} variant="danger" onClick={() => { deleteQuestion(_id) }}><FontAwesomeIcon icon="trash" /></Button>
                            : <span>&nbsp;</span>}
                        </td>
                        </tr>
                    )}
                </tbody>
                </Table>
                : <p>No questions to show</p>}
          </div>
      </div>
    </div>
  );
};